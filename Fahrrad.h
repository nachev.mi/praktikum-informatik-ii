#pragma once

#include "Fahrzeug.h"

class Fahrrad : public Fahrzeug
{
public:
    Fahrrad(std::string sName, double dMaxGeschwindigkeit, int id = 0);
    double dGeschwindigkeit() const override;
    void vSimulieren() override;
    void vAusgeben(std::ostream&) const override;
    void vAusgeben() const override;
    virtual  void vZeichnen(const Weg& )  override;
};
