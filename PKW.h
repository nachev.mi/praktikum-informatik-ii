#pragma once

#include "Fahrzeug.h"

class PKW : public Fahrzeug
{
private:
    double p_dVerbrauch;
    const double p_dTankvolumen;
    double p_dTankinhalt;

public:
    PKW();
    PKW(std::string sName, double dMaxGeschwindigkeit, double dVerbrauch, double dTankvolumen = 55., int id = 0);
    virtual ~PKW();
    void vAusgeben() const override;
    void vAusgeben(std::ostream&) const override;
    void vSimulieren() override;
    virtual double dTanken(double dMenge) override;
    void operator=(PKW& pkw);
    double dGeschwindigkeit() const override;
    virtual  void vZeichnen(const Weg& rweg)  override;
};
