#include "Kreuzung.h"
#include "Weg.h"


Kreuzung::Kreuzung(std::string sName, double dTank) : Simulationsobjekt(), p_dTankstelle(dTank)
{}
Kreuzung::~Kreuzung()
{
}
void Kreuzung::vTanken(Fahrzeug& fahrzeug)
{
	if (p_dTankstelle > 0.)
	{
		double kolichestvo = fahrzeug.dTanken();
		p_dTankstelle = p_dTankstelle - kolichestvo;
	}
	else p_dTankstelle == 0.;

}
void Kreuzung::vVerbinde(std::string hinname, std::string ruckname, double weglange, std::shared_ptr<Kreuzung> kreuzung1, std::shared_ptr<Kreuzung> kreuzung2, bool p_dUeberholverbot, Tempolimit tempolimit)
{
	std::shared_ptr<Weg> hinweg = std::make_shared<Weg>(hinname, weglange, p_dUeberholverbot, Tempolimit::Autobahn); 
	std::shared_ptr<Weg> ruckweg = std::make_shared<Weg>(ruckname, weglange, p_dUeberholverbot, Tempolimit::Autobahn); 

	std::shared_ptr<Weg> hhinweg = hinweg;
	std::shared_ptr<Weg> rruckweg = ruckweg;

	std::shared_ptr<Kreuzung> kreuzung11 = kreuzung1;
	std::shared_ptr<Kreuzung> kreuzung22 = kreuzung2;

	hinweg->setWeg(rruckweg);
	ruckweg->setWeg(hhinweg);

	hinweg->setKreuzung(kreuzung22); 
	ruckweg->setKreuzung(kreuzung11);

	kreuzung1->p_pWege.push_back(hinweg); 
	kreuzung2->p_pWege.push_back(ruckweg); 

}

void Kreuzung::vAnnahme(std::unique_ptr<Fahrzeug> fahrzeug, double zeit )
{
	fahrzeug->dTanken();//Tanken
	p_pWege.front()->vAnnahme(std::move(fahrzeug), zeit); 
}

void Kreuzung::vSimulieren()
{
	for (auto i = p_pWege.begin(); i != p_pWege.end();)
	{
		(*i)->vSimulieren();
	}
}

std::shared_ptr<Weg> Kreuzung::pZufaelligerWeg(Weg& weg)
{
	static std::mt19937 device(0);
	std::uniform_int_distribution<int> dist(1, p_pWege.size());

	if (p_pWege.size() == 1)
	{
		return p_pWege.front();
	}
	else
	{
		while (true) //Unendlich Schleife 
		{
			int K = dist(device); //Zufallszahl 
			int i = 1;
			std::shared_ptr<Weg> neuweg;

			for (auto it = p_pWege.begin(); it != p_pWege.end(); it++, i++)
			{
				if (i == K)
				{
					neuweg = *it;
					break;
				}
			}
				return neuweg; 
			
		}

	}
}
void Kreuzung::vZeichnen()
{
	for (auto it = p_pWege.begin(); it != p_pWege.end(); it++)
	{
		(*it)->vZeichnen();
	}
}
std::string Kreuzung::getName()
{
	return p_sName;
}
double Kreuzung::getTank()
{
	return p_dTankstelle;
}

