#include "Parken.h"
#include "Fahren.h"
#include "Losfahren.h"

Parken::Parken(Weg &weg, double dStartzeitpunkt) : Verhalten(weg), p_dStartzeitpunkt(dStartzeitpunkt) {}

double Parken::dStrecke(Fahrzeug &aFzg, double dZeitIntervall) {
    if(dGlobaleZeit <= p_dStartzeitpunkt - 0.00001) {
        return 0.;
    }
    else {
        throw Losfahren(aFzg, p_pWeg);
    }
}