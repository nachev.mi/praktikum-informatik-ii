#include "Fahrrad.h"
#include "Fahrausnahme.h"
#include "Fahren.h"
#include "Parken.h"
#include "SimuClient.h"
Fahrrad::Fahrrad(std::string sName, double dMaxGeschwindigkeit, int id) : Fahrzeug(sName, dMaxGeschwindigkeit, id) {}

double Fahrrad::dGeschwindigkeit() const {
    double dGeschwindigkeit = p_dMaxGeschwindigkeit * pow(0.9, ((int)p_dGesamtStrecke) / 20);
    return dGeschwindigkeit >= 12. ? dGeschwindigkeit : 12.;
}

void Fahrrad::vSimulieren() {
    double dZuruckgelegteZeit = dGlobaleZeit - p_dZeit;
    p_dGesamtZeit += dZuruckgelegteZeit;
    p_dZeit = dGlobaleZeit;

    try {
        double dLetzteStrecke = p_pVerhalten->dStrecke(*this, dZuruckgelegteZeit);
        p_dGesamtStrecke += dLetzteStrecke;
        p_dAbschnittStrecke += dLetzteStrecke;
    }
    catch (Fahrausnahme& eAusnahme) {
        eAusnahme.vBearbeiten();
    }
}

void Fahrrad::vAusgeben() const {
    this->Fahrzeug::vAusgeben();
    std::cout << std::endl;
}

void Fahrrad::vAusgeben(std::ostream& o) const {
    this->Fahrzeug::vAusgeben(o);
    o << std::endl;
}
void Fahrrad::vZeichnen(const Weg& rWeg) 
{
    double RelPosition = this->dGetAbschnittstrecke() / rWeg.llong();	//realtive Position des PKWs zur Laenge des Weges: 
    bZeichneFahrrad(this->Namees(), rWeg.getName(), RelPosition, this->dGeschwindigkeit());
}