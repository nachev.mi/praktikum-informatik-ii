#pragma once
#include "Simulationsobjekt.h"
#include <list>
#include <memory>
#include <string>
#include "Fahrzeug.h"
#include "Tempolimit.h"
#include <random> 
#include <iostream>
//#include "Weg.h"
//#include <fstream> 

class Weg;
class Kreuzung : public Simulationsobjekt {
public:
    Kreuzung() = default;
    Kreuzung(std::string sName, double dTank);
    ~Kreuzung();
    virtual void vTanken(Fahrzeug& fahrzeug);
    static void vVerbinde(std::string hinname, std::string ruckname, double weglange, std::shared_ptr<Kreuzung> kreuzung1, std::shared_ptr<Kreuzung> kreuzung2, bool p_dUeberholverbot = true, Tempolimit tempolimit = Tempolimit::Autobahn);
    void vSimulieren();
    void vAnnahme(std::unique_ptr<Fahrzeug> pFahrzeug, double zeit);
    std::shared_ptr <Weg> pZufaelligerWeg(Weg& weg);
    void vZeichnen();
    std::string getName();
    double getTank();
private:
    double p_dTankstelle;
    std::list<std::shared_ptr<Weg>> p_pWege;
};

