#pragma once

#include <iostream>
#include "memory"
#include <limits>
#include <fstream> 

class Simulationsobjekt {
protected:
    int p_iID = 0;
    double p_dGesamtZeit;
    double p_dZeit;
    std::string p_sName = " ";
public:
    static inline int currentID = 0;
    Simulationsobjekt();
    Simulationsobjekt(int iId, std::string sName);
    ~Simulationsobjekt();
    Simulationsobjekt(Simulationsobjekt&) = delete;
    virtual void vKopf() const;
    static void vKopf(int);
    virtual void vSimulieren() = 0;
    virtual void vAusgeben(std::ostream& o) const;
    virtual void vAusgeben() const;
    bool operator==(Simulationsobjekt& objekt) const;
    std::string sNameAusgabe() {return p_sName;};
    void vEinlesen(std::istream& i);
};

std::ostream& operator<<(std::ostream& o, const std::unique_ptr<Simulationsobjekt>& objekt);
std::ostream& operator<<(std::ostream& o, const Simulationsobjekt& objekt);
std::istream& operator>>(std::istream& i, Simulationsobjekt& objekt);
std::istream& operator>>(std::ostream& i, std::unique_ptr<Simulationsobjekt>& objekt);
