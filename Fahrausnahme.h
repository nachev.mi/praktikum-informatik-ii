#pragma once

#include <iostream>
#include "Fahrzeug.h"
#include "Weg.h"

class Fahrausnahme : public std::exception {
public:
    Fahrausnahme(Fahrzeug& fahrzeug, Weg& weg);
    virtual void vBearbeiten() = 0;
protected:
    Fahrzeug& p_pFahrzeug;
    Weg& p_pWeg;
};

