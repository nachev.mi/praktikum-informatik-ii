#include "Weg.h"
#include "iomanip"
#include "vertagt_liste.h"
#include "Fahrausnahme.h"
#include "Fahrzeug.h"
#include "PKW.h"
#include "Parken.h"
#include "Fahren.h"
#include "Verhalten.h"
#include "Streckenende.h"
#include "Losfahren.h"

Weg::Weg(std::string sName, double dLange, bool p_dUeberholverbot, Tempolimit tempolimit) : Simulationsobjekt(), p_dLange(dLange), p_eTempolimit(tempolimit), p_dUeberholverbot(p_dUeberholverbot){
    p_sName = sName;
    std::shared_ptr<Kreuzung>zielkreuzung = nullptr;
}

double Weg::getTempolimit() {
    if(p_eTempolimit == Tempolimit::Innerorts) {
        return 50.;
    }
    else if (p_eTempolimit == Tempolimit::Landstrasse) {
        return 100.;
    }
    else if (p_eTempolimit == Tempolimit::Autobahn) {
        return std::numeric_limits<double>::max();
    }
    else {
        return 0.;
    }
}

void Weg::vSimulieren() {
    int counter = 0;
    for (auto i = p_pFahrzeuge.begin(); i != p_pFahrzeuge.end();) {
        //p_pFahrzeuge.vAktualisieren();
        try {
            //if (*i != nullptr) {
                (*i)->vSimulieren();
            //}
            //if (*i != nullptr) {
                (*i)->vZeichnen(*this);
            //}
          i++;
        }
        catch (Fahrausnahme& eAusnahme) {
            std::advance(i, 1);
            eAusnahme.vBearbeiten();
           
        }
        /*
        catch (Losfahren& eAusnahme) {
            eAusnahme.vBearbeiten();
            i = p_pFahrzeuge.begin();
        }     
        catch (Streckenende& eAusnahme) {
            std::advance(i, 1);
            eAusnahme.vBearbeiten();
            std::advance(i, -1);
        }
        */
       // p_pFahrzeuge.vAktualisieren();
    }
}

std::string Weg::sNamen() const
{
/*
    vertagt::VListe<std::string> List;
    if (p_pFahrzeuge.empty()) {
        return "";
    }
    for (auto&& i : p_pFahrzeuge)
    {
        List.push_back(i->sNameAusgabe());
    }
    return List;
    */
    if(p_pFahrzeuge.empty()) {
        return "";
    }
    std::string sNamenListe;
    for (auto&& i : p_pFahrzeuge) {
        sNamenListe += i->sNameAusgabe();
        sNamenListe += ", ";
    }
    sNamenListe.pop_back();
    sNamenListe.pop_back();

    return sNamenListe;
   
    /*for (auto&& i : p_pFahrzeuge) {
        vertagt::VListe<std::string> Listovica;
        Listovica.push_back(i->sNameAusgabe());
        Listovica.vAktualisieren();
        for (auto& it : Listovica)
        {
            std::cout << it << std::endl;
        }
    }*/
    
}

void Weg::vAusgeben(std::ostream& o) const {
    o << std::setfill('-');
    o << std::setw(82) << ""  << std::endl;
    o << std::setfill(' ');
    o << std::setprecision(2) << std::fixed << std::setw(2) << p_iID;
    o << std::setw(15) << p_sName;
    o << std::setprecision(2) << std::setw(10) << p_dLange;
    o << std::setw(20) << std::fixed << "(" << sNamen() << ")" << std::endl;

}

void Weg::vKopf() const {
    std::cout << std::setprecision(2) << std::fixed << std::setw(2) << "ID";
    std::cout << std::setw(15) << "Name";
    std::cout << std::fixed << std::setw(10) << "Laenge";
    std::cout << std::setw(20) << "Fahrzeuge" << std::endl;
}

void Weg::vInsert(std::unique_ptr<Fahrzeug>& fahrzeug) {
   // p_pFahrzeuge.insert(p_pFahrzeuge.end(), std::move(fahrzeug));

}

void Weg::vAnnahme(std::unique_ptr<Fahrzeug> fahrzeug) {
    fahrzeug->vNeueStrecke(*this);
    p_pFahrzeuge.push_back(std::move(fahrzeug));
 //   p_pFahrzeuge.vAktualisieren();
}

void Weg::vAnnahme(std::unique_ptr<Fahrzeug> fahrzeug, double dStartzeitpunkt) {
    fahrzeug->vNeueStrecke(*this, dStartzeitpunkt);
    p_pFahrzeuge.push_front(std::move(fahrzeug));
  //  p_pFahrzeuge.vAktualisieren();
}

double Weg::llong() const
{
    return p_dLange;
}
std::string Weg::getName() const
{
    return p_sName;
}
std::unique_ptr<Fahrzeug> Weg::pAbgabe(const Fahrzeug& Fahrz)
{
    for (auto i = p_pFahrzeuge.begin();  i != p_pFahrzeuge.end(); i++) {
        if (**i == Fahrz)
        {            
            auto lokalfzg = std::move(*i);
            p_pFahrzeuge.erase(i);
            //p_pFahrzeuge.vAktualisieren();
            std::cout << "erased" << std::endl;
            return std::move(lokalfzg);
        }
    }
    return nullptr;
}

void Weg::vZeichnen()
{
    if (!p_pFahrzeuge.empty())
    {
        for (auto& it : p_pFahrzeuge)
        {
            it->vZeichnen(*this);
        }
    }
}

double Weg::virtuelleSchranke()
{
    if (p_dUeberholverbot == true)
    {
        return p_dVirtuelleSchranke;
    }
    return p_dLange;
}
void Weg::Schranke(double rastoqnie)
{
    p_dVirtuelleSchranke = rastoqnie;
}
void Weg::setKreuzung(std::shared_ptr<Kreuzung>Zielkreu)
{
    zielkreuzung = Zielkreu;
}
void Weg::setWeg(std::shared_ptr<Weg>ruckweg)
{
    zielweg = ruckweg;
}

std::shared_ptr<Kreuzung> Weg::getKreuzung()
{
    return zielkreuzung;
}

std::shared_ptr<Weg> Weg::getWeg()
{
    return zielweg;
}
