#pragma once

enum class Tempolimit {
    Innerorts, Landstrasse, Autobahn
};