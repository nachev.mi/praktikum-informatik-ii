#include "Losfahren.h"
#include "Fahren.h"
#include "Fahrzeug.h"
#include "PKW.h"
#include "Weg.h"

Losfahren::Losfahren(Fahrzeug &fahrzeug, Weg &weg) : Fahrausnahme(fahrzeug, weg){}

void Losfahren::vBearbeiten() {
    auto neulokalfzg = p_pWeg.pAbgabe(p_pFahrzeug);
    p_pWeg.vAnnahme(std::move(neulokalfzg));
    std::cout << p_pFahrzeug.sNameAusgabe() << " ist auf den Weg " << p_pWeg.sNameAusgabe() << " losgefahren. " << dGlobaleZeit << std::endl;
}