#pragma once

#include "Simulationsobjekt.h"
#include <list>
#include "Fahrzeug.h"
#include "Tempolimit.h"
#include "vertagt_liste.h"
#include "Kreuzung.h"
//#include <fstream>

class Weg : public Simulationsobjekt {
private:
    double p_dLange;
    Tempolimit p_eTempolimit;
    bool p_dUeberholverbot;
public:
    std::list<std::unique_ptr<Fahrzeug>> p_pFahrzeuge;
    //vertagt::VListe<std::unique_ptr<Fahrzeug>> p_pFahrzeuge;
    Weg() = default;
    Weg(std::string sName, double dLange, bool p_dUeberholverbot = true, Tempolimit tempolimit = Tempolimit::Autobahn);
    void vSimulieren() override;
    void vAusgeben(std::ostream& o) const override;
    void vKopf() const override;
    std::string sNamen() const;
    double llong()const;
    virtual double virtuelleSchranke();
    std::string getName()const ;
    void vInsert(std::unique_ptr<Fahrzeug>& fahrzeug);
    double dGetLange() { return p_dLange; };
    void vAnnahme(std::unique_ptr<Fahrzeug> fahrzeug);
    void vAnnahme(std::unique_ptr<Fahrzeug>, double dStartzeitpunkt);
    double getTempolimit();
    void vZeichnen();
    void Schranke(double rastoqnie);
    double p_dVirtuelleSchranke;
    void setWeg(std::shared_ptr<Weg>);
    void setKreuzung(std::shared_ptr<Kreuzung> kreuzung);
    std::shared_ptr<Kreuzung> getKreuzung();
    std::shared_ptr<Weg> getWeg();
    //bool p_dUeberholverbot();
    std::unique_ptr<Fahrzeug> pAbgabe(const Fahrzeug&);
    std::shared_ptr<Weg> zielweg;
    std::shared_ptr<Kreuzung> zielkreuzung;
};