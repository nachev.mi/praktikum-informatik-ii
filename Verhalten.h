#pragma once

#include "Weg.h"
#include "Fahrzeug.h"

class Verhalten {
public:
    Verhalten(Weg& weg);
    virtual double dStrecke(Fahrzeug& aFzg, double dZeitIntervall) = 0;
    Weg& pGetWeg() {return p_pWeg;};

protected:
    Weg& p_pWeg;
};
