#include "Streckenende.h"


Streckenende::Streckenende(Fahrzeug &fahrzeug, Weg &weg) : Fahrausnahme(fahrzeug, weg) {}

void Streckenende::vBearbeiten() {
    std::cout << p_pFahrzeug.sNameAusgabe() << " hat das Ende des Weges " << p_pWeg.sNameAusgabe() << " erreicht." << std::endl;
   // p_pWeg.pAbgabe(p_pFahrzeug);
    std::shared_ptr<Kreuzung> Kreuzuung = p_pWeg.getKreuzung();
    std::unique_ptr<Fahrzeug> fahrzeugkcopy = p_pWeg.pAbgabe(p_pFahrzeug);
    std::shared_ptr<Weg>weg = Kreuzuung->pZufaelligerWeg(p_pWeg);
    Kreuzuung->vTanken(*fahrzeugkcopy);
    weg->vAnnahme(move(fahrzeugkcopy));
    std::cout <<  std::setw(20) << "ZEIT" << std::setw(40) << ": " << dGlobaleZeit << std::endl
              <<  std::setw(20) << "KREUZUNG" << std::setw(40) << ": " + Kreuzuung->getName() << Kreuzuung->getTank()<< std::endl
              << std::setw(20) << "WECHSEL" << std::setw(40) << ": " + p_pWeg.getName() << " -> " << weg->getName() << std::endl
              << std::setw(20) << "FAHRZEUG" << std::setw(40) << ": " + p_pFahrzeug.Namees() << std::endl;
}
