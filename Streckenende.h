#pragma once
#include "Fahrausnahme.h"
#include "SimuClient.h"
#include "Kreuzung.h"
#include <iomanip>
#include "Weg.h"

class Streckenende : public Fahrausnahme {
public:
    Streckenende(Fahrzeug& fahrzeug, Weg& weg);
    void vBearbeiten() override;
};
