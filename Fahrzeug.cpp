#include "Fahrzeug.h"
#include <iomanip>
//#include <utility>
#include "Fahren.h"
#include "Parken.h"
#include "Weg.h"
#include "Losfahren.h"
#include "Streckenende.h"

Fahrzeug::Fahrzeug(std::string name, double max, int id) : Simulationsobjekt(currentID + 1, name), p_dMaxGeschwindigkeit(max), p_dGesamtStrecke()
{
}

Fahrzeug::~Fahrzeug()
{
//	std::cout << "Fahrzeug " << p_iID << " mit Namen " << p_sName << " loshen." << std::endl;
}

void Fahrzeug::vAusgeben() const
{
    this->Simulationsobjekt::vAusgeben();
    std::cout << std::fixed << std::setw(25) << dGeschwindigkeit();
    std::cout << std::setprecision(2) << std::fixed << std::setw(20) << p_dGesamtStrecke;
}

void Fahrzeug::vAusgeben(std::ostream& o) const
{
    this->Simulationsobjekt::vAusgeben(o);
    o << std::fixed << std::setw(25) << dGeschwindigkeit();
    o << std::setprecision(2) << std::fixed << std::setw(20) << p_dGesamtStrecke;
}

void Fahrzeug::vSimulieren()
{
    double dZuruckgelegteZeit = dGlobaleZeit - p_dZeit;
    p_dGesamtZeit += dZuruckgelegteZeit;
    p_dZeit = dGlobaleZeit;

    try {
        double dLetzteStrecke = p_pVerhalten->dStrecke(*this, dZuruckgelegteZeit);
        p_dGesamtStrecke += dLetzteStrecke;
        p_dAbschnittStrecke += dLetzteStrecke;
        std::cout << p_dAbschnittStrecke << std::endl;
        if (p_dAbschnittStrecke != 0)
        {
            p_pVerhalten->pGetWeg().Schranke(p_dAbschnittStrecke);
         //   std::cout << p_dAbschnittStrecke << std::endl;
        }
    }
    catch (Fahrausnahme& eAusnahme) {
       eAusnahme.vBearbeiten();
    }
}

double Fahrzeug::dTanken(double dMenge) {
    return 0.0f;
}

bool Fahrzeug::operator<(Fahrzeug fahrzeug) const {
    return p_dGesamtStrecke < fahrzeug.p_dGesamtStrecke ? true : false;
}

void Fahrzeug::operator=(const Fahrzeug& fahrzeug) {
    p_sName = fahrzeug.p_sName;
    p_dMaxGeschwindigkeit = fahrzeug.p_dMaxGeschwindigkeit;
    p_iID = fahrzeug.p_iID;
}
bool Fahrzeug::operator==(const Fahrzeug& fahrzeug)
{
    if (p_iID == fahrzeug.p_iID) {
        return true;
    }
    else return false;
}

void Fahrzeug::vNeueStrecke(Weg &weg) {
    p_dAbschnittStrecke = 0.;
    auto verhalten = std::make_shared<Fahren>(weg);
    p_pVerhalten = verhalten;
}

void Fahrzeug::vNeueStrecke(Weg &weg, double dStartzeitpunkt) {
    p_dAbschnittStrecke = 0.;
    auto verhalten = std::make_shared<Parken>(weg, dStartzeitpunkt);
    p_pVerhalten = verhalten;
}
void Fahrzeug::vZeichnen(const Weg&)
{

}

double Fahrzeug::AbschnittStrecke()
{
    if (p_dAbschnittStrecke != 0) {
        return p_dAbschnittStrecke;
    }
    else { return -500; }
}
std::string Fahrzeug::Namees()
{
    return p_sName;
}
/*
std::ostream& operator<<(std::ostream& o, const std::unique_ptr<Fahrzeug>& fahrzeug) {
    fahrzeug->vAusgeben(o);
    return o;
}

std::ostream& operator<<(std::ostream& o, const Fahrzeug& fahrzeug) {
    fahrzeug.vAusgeben(o);
    return o;
}*/