#include "PKW.h"
#include <iomanip>
#include "Fahren.h"
#include "Parken.h"
#include "Fahrausnahme.h"
#include "SimuClient.h"

PKW::PKW(std::string sName, double dMaxGeschwindigkeit, double dVerbrauch, double dTankvolumen, int id) : Fahrzeug(sName, dMaxGeschwindigkeit, id),  p_dVerbrauch(dVerbrauch), p_dTankvolumen(dTankvolumen), p_dTankinhalt(p_dTankvolumen / 2.)
{}

PKW::~PKW()
{}

void PKW::vAusgeben() const {
    this->Fahrzeug::vAusgeben();
    std::cout << std::setprecision(2) << std::fixed << std::setw(20) << p_dGesamtStrecke / 100 * p_dVerbrauch << std::endl;
}

void PKW::vAusgeben(std::ostream& o) const {
    this->Fahrzeug::vAusgeben(o);
    o << std::setprecision(2) << std::fixed << std::setw(20) << p_dGesamtStrecke / 100 * p_dVerbrauch << std::endl;
    //o << p_dTankinhalt;
}

double PKW::dTanken(double dMenge = std::numeric_limits<double>::infinity()) {
    double dAktMenge;
    if (p_dTankinhalt + dMenge > p_dTankvolumen || dMenge < 0.) {
        dAktMenge = dMenge - (p_dTankinhalt + dMenge - p_dTankvolumen);
        p_dTankinhalt = p_dTankvolumen;
    }
    else {
        p_dTankinhalt = p_dTankinhalt + dMenge;
        dAktMenge = dMenge;
    }
    return dAktMenge;
}

void PKW::vSimulieren()
{
    double dZuruckgelegteZeit = dGlobaleZeit - p_dZeit;
    p_dGesamtZeit += dZuruckgelegteZeit;
    p_dZeit = dGlobaleZeit;

  // try {
        if (p_dTankinhalt > 0.) {
            double dLetzteStrecke = p_pVerhalten->dStrecke(*this, dZuruckgelegteZeit);
            double dMomentanVerbrauch = dLetzteStrecke * p_dVerbrauch / 100;
            p_dGesamtStrecke += dLetzteStrecke;
            p_dAbschnittStrecke += dLetzteStrecke;
            p_dTankinhalt = p_dTankinhalt - dMomentanVerbrauch > 0 ? p_dTankinhalt - dMomentanVerbrauch : 0.;
            if (p_dAbschnittStrecke != 0)
            {
                p_pVerhalten->pGetWeg().Schranke(p_dAbschnittStrecke);
                std::cout << p_dAbschnittStrecke << std::endl;
            }
        }
   // }
  //  catch (Fahrausnahme& eAusnahme) {
     //   eAusnahme.vBearbeiten();
   //}
}

double PKW::dGeschwindigkeit() const {
    double dTempolimit = p_pVerhalten->pGetWeg().getTempolimit();
    return dTempolimit < p_dMaxGeschwindigkeit ? dTempolimit : p_dMaxGeschwindigkeit;
}

void PKW::operator=(PKW& pkw) {
    p_sName = pkw.p_sName;
    p_dMaxGeschwindigkeit = pkw.p_dMaxGeschwindigkeit;
    p_iID = pkw.p_iID;
    p_dVerbrauch = pkw.p_dVerbrauch;
    p_dTankinhalt = pkw.p_dTankinhalt;
    p_dGesamtStrecke = pkw.p_dGesamtStrecke;
    p_dGesamtZeit = pkw.p_dGesamtZeit;
    p_dZeit = pkw.p_dZeit;
    p_dAbschnittStrecke = pkw.p_dAbschnittStrecke;
}

void PKW::vZeichnen(const Weg& rWeg)
{
    double RelPosition = this->dGetAbschnittstrecke() / rWeg.llong();	//realtive Position des PKWs zur Laenge des Weges: 
    bZeichnePKW(this->Namees(), rWeg.getName(), RelPosition, this->dGeschwindigkeit(), this->p_dTankinhalt);
}