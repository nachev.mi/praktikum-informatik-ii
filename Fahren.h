#pragma once

#include "Verhalten.h"

class Fahren : public Verhalten {
public:
    Fahren(Weg& weg);
    double dStrecke(Fahrzeug& aFzg, double dZeitIntervall) override;
};

