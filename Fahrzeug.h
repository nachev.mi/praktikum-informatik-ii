#pragma once

#include <cmath>
#include "Simulationsobjekt.h"


extern double dGlobaleZeit;

class Verhalten;
class Weg;

class Fahrzeug : public Simulationsobjekt
{
public:
    Fahrzeug() = default;
    Fahrzeug(std::string p_sName, double p_dMaxGeschwindigkeit, int id = 0);
    virtual ~Fahrzeug();
    virtual void vAusgeben(std::ostream& o) const;
    virtual void vAusgeben() const;
    virtual void vSimulieren();
    virtual double dTanken(double dMenge = std::numeric_limits<double>::infinity());
    virtual double dGeschwindigkeit() const {return p_dMaxGeschwindigkeit;};
    virtual void vNeueStrecke(Weg& weg);
    virtual void vNeueStrecke(Weg& weg, double dStartzeitpunkt);
    double dGetAbschnittstrecke() const {return p_dAbschnittStrecke;};
    void vSetAbschnittStrecke(double dStrecke) {p_dAbschnittStrecke = dStrecke;};
    virtual bool operator<(Fahrzeug fahrzeug) const;
    virtual void operator=(const Fahrzeug& fahrzeug);
    virtual bool operator==(const Fahrzeug& fahrzeug);
    virtual void vZeichnen(const Weg&);
    virtual double AbschnittStrecke();
    void vSetVerhalten(std::shared_ptr<Verhalten> verhalten) { p_pVerhalten = std::move(verhalten); };
    virtual std::string Namees();
protected:
    double p_dGesamtStrecke;
    double p_dMaxGeschwindigkeit;
    double p_dAbschnittStrecke;
    std::shared_ptr<Verhalten> p_pVerhalten;
};
//std::ostream& operator<<(std::ostream& o, const std::unique_ptr<Fahrzeug>& fahrzeug);
//std::ostream& operator<<(std::ostream& o, const Fahrzeug& fahrzeug);
