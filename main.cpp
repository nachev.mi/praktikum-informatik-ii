#include <iostream>
#include "PKW.h"
#include <memory>
#include <sstream>
#include <vector>
#include <iomanip>
#include "SimuClient.h"
#include "Fahrrad.h"
#include "Weg.h"
#include <random>
#include "vertagt_liste.h"

double  dGlobaleZeit= 0.;

double dEpsilon = 0.001;

void vAufgabe_2() {
    int iPKWAnzahl, iFRAnzahl, iId;
    iId = 0;
    std::vector<std::unique_ptr<Fahrzeug>> fahrzeugeVect;
    std::cout << "Wie viele PKWs moechten Sie erzeugen?" << std::endl;
    std::cin >> iPKWAnzahl;
    std::cout << "Wie viele Fahrraeder moechten Sie erzeugen?" << std::endl;
    std::cin >> iFRAnzahl;
    for (int i = 0; i < iPKWAnzahl; i++) {
        std::unique_ptr<PKW> newPKW = std::make_unique<PKW>("PKW" + std::to_string(i), 140., 10., 40., iId);
        fahrzeugeVect.push_back(std::move(newPKW));
        iId++;
    }
    for (int i = 0; i < iFRAnzahl; i++) {
        std::unique_ptr<Fahrrad> newFahrrad = std::make_unique<Fahrrad>("FR" + std::to_string(i), 25., iId);
        fahrzeugeVect.push_back(std::move(newFahrrad));
        iId++;
    }
    fahrzeugeVect[0]->Simulationsobjekt::vKopf();
    while (dGlobaleZeit < 15.) {
        if (dGlobaleZeit >= 3. && dGlobaleZeit <= 3.49) {
            for (auto i = fahrzeugeVect.begin(); i != fahrzeugeVect.end(); i++) {
                (*i)->dTanken();
            }
        }
        for (auto i = fahrzeugeVect.begin(); i != fahrzeugeVect.end(); i++) {
            (*i)->vSimulieren();
            std::cout << *i;
        }
        dGlobaleZeit += 0.5;
    }
}



void vAufgabe_AB1() {

    int l = 0; // Laufindex für gezielte AUsgabe
    std::vector<int> ausgabe{ 15 };
    double dTakt = 0.3;

    std::vector<std::unique_ptr<Fahrzeug>> vecFahrzeuge;
    vecFahrzeuge.push_back(std::make_unique <PKW>("Audi", 217, 10.7));
    vecFahrzeuge.push_back(std::make_unique <Fahrrad>("BMX", 21.4));
    for (dGlobaleZeit = 0; dGlobaleZeit < 6; dGlobaleZeit += dTakt)
    {
        auto itL = find(ausgabe.begin(), ausgabe.end(), l);
        if (itL != ausgabe.end()) {
            std::cout << std::endl << l << " Globalezeit = " << dGlobaleZeit << std::endl;
            vecFahrzeuge[0]->Simulationsobjekt::vKopf();
        }

        for (int i = 0; i < vecFahrzeuge.size(); i++)
        {
            vecFahrzeuge[i]->vSimulieren();
            if (fabs(dGlobaleZeit - 3.0) < dTakt / 2)
            {
                vecFahrzeuge[i]->dTanken();
            }
            if (itL != ausgabe.end()) {
                std::cout << *vecFahrzeuge[i] << std::endl;
            }
        }
        l++;
    }
    char c;
    std::cin >> c;
}

void vAufgabe_AB2() {

    int l = 0; // Laufindex für gezielte AUsgabe
    std::vector<int> ausgabe{ 15 };
    double dTakt = 0.3;

    std::vector<std::unique_ptr<Fahrzeug>> vecFahrzeuge;
    vecFahrzeuge.push_back(std::make_unique <PKW>("Audi", 217, 10.7));
    vecFahrzeuge.push_back(std::make_unique <Fahrrad>("BMX", 21.4));
    for (dGlobaleZeit = 0; dGlobaleZeit < 6; dGlobaleZeit += dTakt)
    {
        auto itL = find(ausgabe.begin(), ausgabe.end(), l);
        if (itL != ausgabe.end()) {
            std::cout << std::endl << l << " Globalezeit = " << dGlobaleZeit << std::endl;
            vecFahrzeuge[0]->Simulationsobjekt::vKopf();
        }

        for (int i = 0; i < vecFahrzeuge.size(); i++)
        {
            vecFahrzeuge[i]->vSimulieren();
            if (fabs(dGlobaleZeit - 3.0) < dTakt / 2)
            {
                vecFahrzeuge[i]->dTanken();
            }
            if (itL != ausgabe.end()) {
                std::cout << *vecFahrzeuge[i] << std::endl;
            }
        }
        l++;
    }
    char c;
    std::cin >> c;
}

void vAufgabe_Probe() {
    Fahrzeug* pF1 = new PKW("Audi", 150, 8);
    dGlobaleZeit = 0.0;
    pF1->Simulationsobjekt::vKopf();
    dGlobaleZeit = 3.0;
    std::cout << std::endl << "Globalezeit = " << dGlobaleZeit << std::endl;
    pF1->vSimulieren();
    std::cout << *pF1 << std::endl;
    delete pF1;
    char c;
    std::cin >> c;
}

void vAufgabe_4() {
    auto f1 = std::make_unique<Fahrzeug>("Auto 1", 120.);
    auto f2 = std::make_unique<Fahrzeug>("Auto 2", 130.);
    Weg w1("Weg 1", 1000.);
    w1.vInsert(f1);
    w1.vInsert(f2);
    w1.vKopf();
    std::cout << w1;
}

void vAufgabe_5() {
    auto f1 = std::make_unique<Fahrzeug>("F1", 100.);
    auto f2 = std::make_unique<Fahrzeug>("F2", 110.);
    auto f3 = std::make_unique<Fahrzeug>("F3", 120.);
    Weg weg("W1", 1200.);
    weg.vAnnahme(std::move(f1));
    weg.vAnnahme(std::move(f2));
    weg.vAnnahme(std::move(f3));
    weg.vKopf();
    weg.vSimulieren();
    for (auto& elem : weg.p_pFahrzeuge) {
        std::cout << elem << std::endl;
    }
        
    //std::cout << weg;
}

void vAufgabe_6() {


    bInitialisiereGrafik(800, 500);
    int wegKoordinaten[4] = { 700 , 250 , 100 , 250 };
  //  Weg weg1("W1", 280., Tempolimit::Autobahn);
  //   Weg weg2("W1", 1200., Tempolimit::Innerorts);
   // vertagt::VListe<Fahrzeug> Listovica;
    auto f1 = std::make_unique<Fahrrad>("F1", 300.);
    Weg weg3("hinfahrt", 500.,true, Tempolimit::Autobahn);
    Weg weg4("Ruckfahrt", 500., true, Tempolimit::Autobahn);
    bZeichneStrasse(weg3.getName(), weg4.getName(), weg3.llong(), 2, wegKoordinaten);
    auto f2 = std::make_unique<PKW>("F2", 100., 10.);
    auto f3 = std::make_unique<PKW>("F3", 200., 1.);
   // auto f4 = std::make_unique<PKW>("F4", 130., 1.);
   // auto f5 = std::make_unique<PKW>("F5", 10., 1.);
    //auto f6 = std::make_unique<PKW>("F6", 20., 1.);
   // auto f7 = std::make_unique<PKW>("F7", 10., 1.);
    weg3.vAnnahme(std::move(f1));
    weg4.vAnnahme(std::move(f2) );
    weg4.vAnnahme(std::move(f3));
   // weg4.vAnnahme(std::move(f4));
    //weg4.vAnnahme(std::move(f5));
   // weg4.vAnnahme(std::move(f6), 2.);
   // weg4.vAnnahme(std::move(f7));
    

    Simulationsobjekt::vKopf(0);
    while (dGlobaleZeit <= 7) {
        auto iter = weg3.p_pFahrzeuge.begin();
        auto iter2 = weg4.p_pFahrzeuge.begin();
        weg3.vSimulieren();
        (*iter)->vAusgeben();
        weg4.vSimulieren();
        for (auto i = weg4.p_pFahrzeuge.begin(); i != weg4.p_pFahrzeuge.end(); i++) {
             (*i)->vAusgeben();
        }
        dGlobaleZeit += 0.5;
        vSleep(100);
    }
    /*
    for (auto i = weg4.p_pFahrzeuge.begin(); i != weg4.p_pFahrzeuge.end(); i++) {
        std::cout << *i << std::endl;
    }
    */
    vSleep(5000);
    vBeendeGrafik();
}
void vAufgabe_6a()
{
    vertagt::VListe<int> Listche;
    for (int i=0; i<= 10; i++)
    {
        int seed = 0, a = 0, b = 10;
        static std::mt19937 device(seed);
        std::uniform_int_distribution<int> dist(a, b);
        int zuf = dist(device);
        std::cout << zuf << " ";
        Listche.push_back(zuf);
        Listche.vAktualisieren();
    }
    std::cout << std::endl;
    for (auto& it : Listche)
    {
        std::cout << it ;
    }
    std::cout<< std::endl;
    for (auto iter = Listche.begin(); iter != Listche.end(); iter++)
    {
        if (*iter > 5)
        {
            Listche.erase(iter);
        }
    }
    for (auto& it : Listche)
    {
        std::cout << it << std::endl;
    }
    std::cout <<std::endl<<"aktualisiert:" << std::endl;
    Listche.vAktualisieren();
   
    for (auto& it : Listche)
    {
        std::cout << it ;
    }
    int k = 1 + (rand() % 10);
    Listche.push_back(k);
    int l = 1 + (rand() % 10);
    Listche.push_front(l);
    Listche.vAktualisieren();
    std::cout << std::endl << "aktualisiert plus 2 zufallige am Anfang und am ende:" << std::endl;
    for (auto& it : Listche)
    {
        std::cout << it  ;
    }
}

void vAufgabe_7() {
    auto kreuzung1 = std::make_shared<Kreuzung>("Kreuzung1", 0);
    auto kreuzung2 = std::make_shared<Kreuzung>("Kreuzung2", 1000);
    auto kreuzung3 = std::make_shared<Kreuzung>("Kreuzung3", 0);
    auto kreuzung4 = std::make_shared<Kreuzung>("Kreuzung4", 0);

    Kreuzung::vVerbinde("Str1", "Str1R", 40, kreuzung1, kreuzung2, true, Tempolimit::Innerorts);
    Kreuzung::vVerbinde("Str2", "Str2R", 115, kreuzung2, kreuzung3, false);
    Kreuzung::vVerbinde("Str3", "Str3R", 40, kreuzung2, kreuzung3, true, Tempolimit::Innerorts);
    Kreuzung::vVerbinde("Str4", "Str4R", 55, kreuzung4, kreuzung2,true, Tempolimit::Innerorts);
    Kreuzung::vVerbinde("Str5", "Str5R", 85, kreuzung4, kreuzung3, false);
    Kreuzung::vVerbinde("Str6", "Str6R", 130, kreuzung4, kreuzung4, false, Tempolimit::Landstrasse);

    int iKoordinatenStr1[] = { 680,40,680,300 };
    int iKoordinatenStr2[] = { 680,300,850,300,970,390,970,500,850,570,680,570 };
    int iKoordinatenStr3[] = { 680,300,680,570 };
    int iKoordinatenStr4[] = { 320,300,680,300 };
    int iKoordinatenStr5[] = { 320,300,320,420,350,510,500,570,680,570 };
    int iKoordinatenStr6[] = { 320,300,320,150,200,60,80,90,70,250,170,300,320,300 };

    bInitialisiereGrafik(1000, 600);
    bZeichneStrasse("Str1", "Str1R", 40, 2, iKoordinatenStr1);
    bZeichneStrasse("Str2", "Str2R", 115, 6, iKoordinatenStr2);
    bZeichneStrasse("Str3", "Str3R", 40, 2, iKoordinatenStr3);
    bZeichneStrasse("Str4", "Str4R", 55, 2, iKoordinatenStr4);
    bZeichneStrasse("Str5", "Str5R", 85, 5, iKoordinatenStr5);
    bZeichneStrasse("Str6", "Str6R", 130, 7, iKoordinatenStr6);
    bZeichneKreuzung(680, 40);
    bZeichneKreuzung(680, 300);
    bZeichneKreuzung(680, 570);
    bZeichneKreuzung(320, 300);

    auto fahrzeug1i = std::make_unique<PKW>("PKW1i", 140, 7, 50);
    auto fahrzeug2i = std::make_unique<PKW>("PKW2i", 60, 7.4, 30);
    auto fahrzeug3i = std::make_unique<PKW>("PKW3i", 80, 7.6, 34);
    auto fahrzeug1l = std::make_unique<PKW>("PKW1l", 140, 7, 50);
    auto fahrzeug2l = std::make_unique<PKW>("PKW2l", 60, 7.4, 30);
    auto fahrzeug3l = std::make_unique<PKW>("PKW3l", 80, 7.6, 34);
    kreuzung1->vAnnahme(std::move(fahrzeug1i), 1.5);
    kreuzung2->vAnnahme(std::move(fahrzeug2i), 1.25);
    kreuzung3->vAnnahme(std::move(fahrzeug3i), 1);
    kreuzung4->vAnnahme(std::move(fahrzeug1l), 1.75);
    kreuzung3->vAnnahme(std::move(fahrzeug2l), 0.5);
    kreuzung4->vAnnahme(std::move(fahrzeug3l), 0.75);

    auto fahrzeug4i = std::make_unique<PKW>("PKW4i", 50, 7, 50);
    auto fahrzeug5i = std::make_unique<PKW>("PKW5i", 60, 7.4, 30);
    auto fahrzeug6i = std::make_unique<PKW>("PKW6i", 80, 7.6, 34);
    auto fahrzeug4l = std::make_unique<PKW>("PKW4l", 50, 7, 50);
    auto fahrzeug5l = std::make_unique<PKW>("PKW5l", 60, 7.4, 30);
    auto fahrzeug6l = std::make_unique<PKW>("PKW6l", 80, 7.6, 34);
    auto fahrzeug7l = std::make_unique<Fahrrad>("Fahrrad7l", 60);
    kreuzung1->vAnnahme(std::move(fahrzeug4i), 2);
    kreuzung2->vAnnahme(std::move(fahrzeug5i), 2.5);
    kreuzung2->vAnnahme(std::move(fahrzeug6i), 2.25);
    kreuzung1->vAnnahme(std::move(fahrzeug4l), 2.75);
    kreuzung3->vAnnahme(std::move(fahrzeug5l), 3);
    kreuzung4->vAnnahme(std::move(fahrzeug6l), 3.5);
    kreuzung1->vAnnahme(std::move(fahrzeug7l), 0.25);
    for (int i = 0; i < 100; i++)
    {
        kreuzung1->vSimulieren();
        kreuzung2->vSimulieren();
        kreuzung3->vSimulieren();
        kreuzung4->vSimulieren();
        kreuzung1->vZeichnen();
        kreuzung2->vZeichnen();
        kreuzung3->vZeichnen();
        kreuzung4->vZeichnen();
        vSetzeZeit(dGlobaleZeit);
        std::cout << "Aktuelle Zeit" << dGlobaleZeit << std::endl;
        dGlobaleZeit += 1;
    }
    vBeendeGrafik();
}

void vAufgabe_8() {
    std::ifstream fin("VO.dat");
    fin.exceptions(std::ios::eofbit | std::ios::failbit | std::ios::badbit);
}

int main() {
    vAufgabe_7();

    //vAufgabe_1a();
    //std::unique_ptr<Fahrzeug> palatka = std::make_unique<Fahrzeug>("kuro", 54);
    //	std::shared_ptr<Fahrzeug> ne_palatka = std::make_shared <Fahrzeug>();
    //palatka->vSimulieren();
    //palatka->vSimulieren();
    //palatka->vKopf();
    //palatka->vAusgeben();


    //Fahrzeug *a =new Fahrzeug("kur" , 10);
    //delete a;
    return 0;
}
/*
void vAufgabe_1a()
{
	std::vector<std::unique_ptr<Fahrzeug>> Fahrzeug_vector;
	std::unique_ptr<Fahrzeug> palatka = std::make_unique<Fahrzeug>("M1", 10);
	std::unique_ptr<Fahrzeug> palatkaa = std::make_unique<Fahrzeug>("M2", 20);
	std::unique_ptr<Fahrzeug> palatkaaa = std::make_unique<Fahrzeug>("M3", 30);
	Fahrzeug_vector.push_back(move(palatka));
	Fahrzeug_vector.push_back(move(palatkaa));
	Fahrzeug_vector.push_back(move(palatkaaa));
	double vreme = 0.0;
	std::cout << "dai interval bratan: ";
	std::cin >> vreme;
	std::cout << std::endl;
	palatkaa->vKopf();
	//for (int i = 0; i < 3; i++)	Fahrzeug_vector[i]->vAusgeben();
	for (dGlobaleZeit = 0; dGlobaleZeit <= 5; dGlobaleZeit += vreme)
	{
		for (int i = 0; i < 3; i++)
		{
			Fahrzeug_vector[i]->vSimulieren();
			Fahrzeug_vector[i]->vAusgeben();

		}
	}
	return 0;
}*/
