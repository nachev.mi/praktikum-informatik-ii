#include "Simulationsobjekt.h"
#include <iomanip>

Simulationsobjekt::Simulationsobjekt() {
    p_iID = Simulationsobjekt::currentID + 1;
    Simulationsobjekt::currentID++;
}

Simulationsobjekt::Simulationsobjekt(int iId, std::string sName)  : p_iID(iId), p_sName(sName)
{
    Simulationsobjekt::currentID++;
}

Simulationsobjekt::~Simulationsobjekt() {
    std::cout << "Objekt geloescht! " << p_sName << std::endl;
}

void Simulationsobjekt::vKopf() const
{
    std::cout << std::setprecision(2) << std::fixed << std::setw(2) << "ID";
    std::cout << std::setw(15) << "Name";
    std::cout << std::setprecision(2) << std::fixed << std::setw(25) << "Akt. Geschwindigkeit";
    std::cout << std::setprecision(2) << std::fixed << std::setw(20) << "Gesamtstrecke";
    std::cout << std::setprecision(2) << std::fixed << std::setw(20) << "Gesamtverbrauch" << std::endl;
}

void Simulationsobjekt::vKopf(int toOverload)
{
    std::cout << std::setprecision(2) << std::fixed << std::setw(2) << "ID";
    std::cout << std::setw(15) << "Name";
    std::cout << std::setprecision(2) << std::fixed << std::setw(25) << "Akt. Geschwindigkeit";
    std::cout << std::setprecision(2) << std::fixed << std::setw(20) << "Gesamtstrecke";
    std::cout << std::setprecision(2) << std::fixed << std::setw(20) << "Gesamtverbrauch" << std::endl;
}

void Simulationsobjekt::vAusgeben() const
{
    std::cout << std::setfill('-');
    std::cout << std::setw(82) << ""  << std::endl;
    std::cout << std::setfill(' ');
    //std::cout<<std::setiosflags;
    std::cout <<std::setprecision(2) << std::fixed << std::setw(2) << p_iID;
    std::cout << std::setw(15) << p_sName;
}

void Simulationsobjekt::vAusgeben(std::ostream& o) const
{
    o << std::setfill('-');
    o << std::setw(82) << "" << std::endl;
    o << std::setfill(' ');
    o << std::setprecision(2) << std::fixed << std::setw(2) << p_iID;
    o << std::setw(15) << p_sName;
}

void Simulationsobjekt::vEinlesen(std::istream& i)
{
    if (p_sName == "")
    {
        i >> p_sName;
    }
    else
    {
        throw std::runtime_error("Kein unbekannte Objekt");
    }
}

bool Simulationsobjekt::operator==(Simulationsobjekt& objekt) const {
    return p_iID == objekt.p_iID;
}

std::ostream& operator<<(std::ostream& o, const std::unique_ptr<Simulationsobjekt>& objekt) {
    objekt->vAusgeben(o);
    return o;
}

std::ostream& operator<<(std::ostream& o, const Simulationsobjekt& objekt) {
    objekt.vAusgeben(o);
    return o;
}

std::istream& operator>>(std::istream& i, Simulationsobjekt& objekt) {
    objekt.vEinlesen(i);
    return i;
}

std::istream& operator>>(std::istream& i, std::unique_ptr<Simulationsobjekt>& objekt) {
    objekt->vEinlesen(i);
    return i;
}
