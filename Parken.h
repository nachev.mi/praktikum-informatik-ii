#pragma once

#include "Verhalten.h"

class Parken : public Verhalten {
public:
    Parken(Weg& weg, double dStartzeitpunkt);
    double dStrecke(Fahrzeug& aFzg, double dZeitIntervall) override;
private:
    double p_dStartzeitpunkt;
};
