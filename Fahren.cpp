#include "Fahren.h"
#include "Streckenende.h"

Fahren::Fahren(Weg & weg) : Verhalten(weg){}

double Fahren::dStrecke(Fahrzeug &aFzg, double dZeitIntervall) {
    double strecke = aFzg.dGeschwindigkeit() * dZeitIntervall;
    if(aFzg.dGetAbschnittstrecke() + strecke <= p_pWeg.dGetLange()) {
        double schranke = p_pWeg.virtuelleSchranke();
        double ostavashtput = abs(schranke - aFzg.dGetAbschnittstrecke());
       if(schranke == 0) ostavashtput = 500.;
       if (ostavashtput == 0) {
           return strecke; 
       }
       if (strecke <= ostavashtput)
       {
           return strecke;
       }
       std::cout <<std::endl<< ostavashtput;
       return ostavashtput;
    }
    else {
        aFzg.vSetAbschnittStrecke(p_pWeg.dGetLange());
       // std::cout << aFzg;
        throw Streckenende(aFzg, p_pWeg);
    }

}